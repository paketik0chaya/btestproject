import React from 'react';
import Comments from "../index";
import {getComments} from "../../../js/actions/Comments";
import {connect} from "react-redux";

class CommentsContainer extends React.Component {
    render() {
        const {preloader, comments} = this.props;
        return (
            <Comments preloader={preloader} comments={comments}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        preloader: state.comments.preloader,
        comments: state.comments.comments,
        error: state.comments.error,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getComments: (id) => dispatch(getComments(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentsContainer);
