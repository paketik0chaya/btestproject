import React from 'react';
import {Badge, ListGroup, ListGroupItem} from 'reactstrap';
import './index.css';

export default function Comments(props) {
    return (
        <ListGroup className="comments-list">
            {
                props.preloader && <span>Loading...</span>
            }
            {
                props.comments && props.comments.map((item, key) => {
                    return (
                        <ListGroupItem key={key} color="info" className='comment'>
                            <Badge color="primary">{item.email}:</Badge>
                            {item.body}
                        </ListGroupItem>
                    )
                })
            }
        </ListGroup>
    )
}