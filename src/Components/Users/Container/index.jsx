import React, {Component} from 'react';
import {getUser} from "../../../js/actions/Users";
import {connect} from 'react-redux';
import Users from "../index";

class UsersContainer extends Component {
    state = {
        users: [],
        error: ''
    };

    componentDidMount() {
        this.props.getUser();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.users !== this.props.users) {
            this.setState({
                users: this.props.users
            });
        }
        if (prevProps.error !== this.state.error) {
            this.setState({
                error: this.props.error
            })
        }
    }

    render() {
        const {preloader, error} = this.props;
        const {users} = this.state;
        return (
            <Users preloader={preloader} users={users} error={error}
                   posts={(id) => this.props.history.push(`/posts/${id}`)}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        preloader: state.users.preloader,
        users: state.users.users,
        error: state.users.error,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getUser: () => dispatch(getUser()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);
