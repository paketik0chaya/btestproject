import React from 'react';
import './index.css'

export default function Users(props) {
    return (
        <div className="users">
            {
                props.preloader && <span>loading...</span>
            }
            {
                props.users && props.users.map((item, key) => {
                    return (
                        <div key={key} onClick={() => props.posts(item.id)}>
                            <span className='username'>{item.username}</span>
                        </div>
                    )
                })
            }
            {
                props.error && <span>{props.error}</span>
            }
        </div>
    )
}