import React from 'react';
import './index.css';

function Header() {
    return <header className="header">
        <span>Simple header</span>
    </header>
}

export default Header;