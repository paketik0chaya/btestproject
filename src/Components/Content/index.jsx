import React, {Component} from 'react';
import './index.css'
import AppRouter from "../../route";

export default class Content extends Component {
    render() {
        return (
            <main className="main">
                <AppRouter/>
            </main>
        )
    }
}