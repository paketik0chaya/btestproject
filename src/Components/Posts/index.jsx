import React from 'react';
import {Button, Card, CardText, CardBody, CardTitle} from 'reactstrap';
import CommentsContainer from "../Comments/Container";
import './index.css'

export default function Posts(props) {
    return (
        <div className="posts">
            {
                props.preloader && <span>Loading...</span>
            }
            {
                props.posts && props.posts.map((item, key) => {
                    return (
                        <Card className="post-card" key={key}>
                            <CardBody>
                                <CardTitle>{item.title}</CardTitle>
                                <CardText>{item.body}</CardText>
                                <Button color="primary" onClick={() => props.showComments(item.id)}>Comments</Button>
                                {
                                    (props.commentShow && props.idPost === item.id) ? <CommentsContainer/> : null
                                }
                            </CardBody>
                        </Card>
                    )
                })
            }
        </div>
    )
}