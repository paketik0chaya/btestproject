import React, {Component} from 'react';
import {getPosts} from "../../../js/actions/Posts";
import {connect} from 'react-redux';
import Posts from "../index";
import {getComments} from "../../../js/actions/Comments";

class PostsContainer extends Component {
    state = {
        commentShow: false,
        idPost: ''
    };

    componentDidMount() {
        this.props.getPosts(this.props.match.params.id)
    }

    showComments = (idPost) => {
        this.props.getComments(idPost);
        if (this.state.idPost !== idPost) {
            this.setState({commentShow: false});
            this.setState({idPost: idPost, commentShow: true})
        } else {
            this.setState({commentShow: !this.state.commentShow});
        }
    };

    render() {
        const {preloader, posts} = this.props;
        const {idPost, commentShow} = this.state;
        return (
            <Posts preloader={preloader} posts={posts} idPost={idPost}
                   showComments={(id) => this.showComments(id)} commentShow={commentShow}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        preloader: state.posts.preloader,
        posts: state.posts.posts,
        error: state.posts.error,
        comments: state.comments.comments
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getPosts: (id) => dispatch(getPosts(id)),
        getComments: (idPost) => dispatch(getComments(idPost))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostsContainer);
