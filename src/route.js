import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import UsersContainer from "./Components/Users/Container";
import PostsContainer from "./Components/Posts/Container";
import {Provider} from "react-redux";
import store from "./js/store";

const AppRouter = () => (
    <Provider store={store}>
        <Router>
            <div>
                <Route path="/" exact component={UsersContainer}/>
                <Route path="/posts/:id" component={PostsContainer}/>
            </div>
        </Router>
    </Provider>
);

export default AppRouter;