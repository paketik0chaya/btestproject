import * as constant from "../../constants/Comments";

const initialState = {
    preloader: false,
    comments: [],
    error: ''
};

export default function comments(state = initialState, action) {
    switch (action.type) {
        case constant.GET_COMMENTS_REQUEST:
            return {
                preloader: true,
                comments: [],
                error: ''
            };
        case constant.GET_COMMENTS_SUCCESS:
            return {
                preloader: false,
                comments: action.comments,
                error: ''
            };
        case constant.GET_COMMENTS_FAILURE:
            return {
                preloader: false,
                comments: [],
                error: action.error,
            };
        default:
            return state
    }
}