import * as constant from "../../constants/Posts";

const initialState = {
    preloader: false,
    posts: [],
    error: ''
};

export default function posts(state = initialState, action) {
    switch (action.type) {
        case constant.GET_POSTS_REQUEST:
            return {
                preloader: true,
                posts: [],
                error: ''
            };
        case constant.GET_POSTS_SUCCESS:
            return {
                preloader: false,
                posts: action.posts,
                error: ''
            };
        case constant.GET_POSTS_FAILURE:
            return {
                preloader: false,
                posts: [],
                error: action.error,
            };
        default:
            return state
    }
}