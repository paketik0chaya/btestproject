import * as constant from "../../constants/Users";

const initialState = {
    preloader: false,
    users: [],
    error: '',
    idUser: 0
};

export default function users(state = initialState, action) {
    switch (action.type) {
        case constant.GET_USERS_REQUEST:
            return {
                preloader: true,
                users: [],
                error: ''
            };
        case constant.GET_USERS_SUCCESS:
            return {
                preloader: false,
                users: action.users,
                error: ''
            };
        case constant.GET_USERS_FAILURE:
            return {
                preloader: false,
                users: [],
                error: action.error,
            };
        default:
            return state
    }
}