import * as constant from '../../constants/Users'
import {request} from '../../../Helpers/API'

export const getUsersRequest = () => ({
    type: constant.GET_USERS_REQUEST,
    preloader: true,
    users: [],
    error: ''
});

export const getUsersSuccess = (users) => ({
    type: constant.GET_USERS_SUCCESS,
    preloader: false,
    users: users,
    error: ''
});

export const getUsersFailure = (error) => ({
    type: constant.GET_USERS_FAILURE,
    preloader: false,
    users: [],
    error: error
});

export const getUser = () => {
    return (dispatch) => {
        dispatch(getUsersRequest());
        request.get('/users')
            .then((response) => dispatch(getUsersSuccess(response.data)))
            .catch((error) => dispatch(getUsersFailure(error)))
    };
};
