import * as constant from '../../constants/Comments'
import {request} from '../../../Helpers/API'

export const getCommentsRequest = () => ({
    type: constant.GET_COMMENTS_REQUEST,
    preloader: true,
    comments: [],
    error: ''
});

export const getCommentsSuccess = (posts) => ({
    type: constant.GET_COMMENTS_SUCCESS,
    preloader: false,
    comments: posts,
    error: ''
});

export const getCommentsFailure = (error) => ({
    type: constant.GET_COMMENTS_FAILURE,
    preloader: false,
    comments: [],
    error: error
});

export const getComments = (idPost) => {
    return (dispatch) => {
        dispatch(getCommentsRequest());
        request.get(`/comments?postId=${idPost}`)
            .then((response) => dispatch(getCommentsSuccess(response.data)))
            .catch((error) => dispatch(getCommentsFailure(error)))
    };
};
