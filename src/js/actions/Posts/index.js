import * as constant from '../../constants/Posts'
import {request} from '../../../Helpers/API'

export const getPostsRequest = () => ({
    type: constant.GET_POSTS_REQUEST,
    preloader: true,
    posts: [],
    error: ''
});

export const getPostsSuccess = (posts) => ({
    type: constant.GET_POSTS_SUCCESS,
    preloader: false,
    posts: posts,
    error: ''
});

export const getPostsFailure = (error) => ({
    type: constant.GET_POSTS_FAILURE,
    preloader: false,
    posts: [],
    error: error
});

export const getPosts = (idUser) => {
    return (dispatch) => {
        dispatch(getPostsRequest());
        request.get(`/posts?userId=${idUser}`)
            .then((response) => dispatch(getPostsSuccess(response.data)))
            .catch((error) => dispatch(getPostsFailure(error)))
    };
};
